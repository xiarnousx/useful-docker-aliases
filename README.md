# Useful Docker Commands Aliases
This repo is an on going updates as I learn more about docker commands I will add more docker managment alias commands. Not limited to docker but also to docker-compose and in the future for swarm or any distributed containers mgmt tools.


## Docker dangling remove images
```alias dd-rm='docker rmi $(docker images -q -f "dangling=true")'```
## Docker container remove containers
```alias dc-rm='docker rm $(docker ps -qa)'```
## Docker stop container
```alias dc-stop='docker stop $(docker ps -qa)'```
## clean up all and remove all dangling intermediate containers
```alias dc-clean='dc-stop && dc-rm && dd-rm'```


Create an alias file say inside ```${HOME}/.docker-src/docker-aliases.sh``` then inside your ```${HOME}/.bashrc``` append the below snippet

```bash
cd ~
vim .bashrc

# Append the below conditional snippet to .bashrc
if [ -f "${HOME}/.docker/docker-aliases.sh" ]; then
    source "${HOME}/.docker/docker-aliases.sh"
fi
```
