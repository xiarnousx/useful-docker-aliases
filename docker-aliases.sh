# Useful Docker Commands Aliases
## Docker dangling remove images
alias dd-rm='docker rmi $(docker images -q -f "dangling=true")'
## Docker container remove containers
alias dc-rm='docker rm $(docker ps -qa)'
## Docker stop container
alias dc-stop='docker stop $(docker ps -qa)'
## clean up all and remove all dangling intermediate containers
alias d-clean='dc-stop && dc-rm && dd-rm'
